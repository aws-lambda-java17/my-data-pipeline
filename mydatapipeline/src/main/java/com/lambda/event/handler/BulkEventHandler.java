package com.lambda.event.handler;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.events.S3Event;
import com.amazonaws.services.lambda.runtime.events.models.s3.S3EventNotification;
import software.amazon.awssdk.core.ResponseBytes;
import software.amazon.awssdk.regions.Region;
import software.amazon.awssdk.services.s3.S3Client;
import software.amazon.awssdk.services.s3.model.GetObjectRequest;
import software.amazon.awssdk.services.s3.model.GetObjectResponse;
import software.amazon.awssdk.services.sns.SnsClient;
import software.amazon.awssdk.services.sns.model.PublishRequest;

import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.List;

public class BulkEventHandler {

    static SnsClient snsClient = SnsClient.builder()
            .region(Region.AP_SOUTHEAST_1)
            .build();

    static S3Client s3 = S3Client.builder()
            .region(Region.AP_SOUTHEAST_1)
            .build();

    public void handleEvents(S3Event event, Context context) {
        String snsTopic = System.getenv("SNS_TOPIC_ARN");

        event.getRecords().forEach(rec -> {
            this.processS3Event(rec, context, snsTopic);
        });

    }

    private List<String> readS3Event(String bucketName, String keyName, Context context) {
        context.getLogger().log("Bucket Name : " + bucketName + "; Key Name : " + keyName);
        List<String> ret = null;
        GetObjectRequest objectRequest = GetObjectRequest
                .builder()
                .key(keyName)
                .bucket(bucketName)
                .build();

        ResponseBytes<GetObjectResponse> objectBytes =  s3.getObjectAsBytes(objectRequest);
        byte[] data = objectBytes.asByteArray();
        String content = new String(data, StandardCharsets.UTF_8);
        String [] line = content.split("\\r?\\n");
        ret = Arrays.asList(line);

        return ret;
    }

    private void processS3Event(S3EventNotification.S3EventNotificationRecord record, Context context, String snsTopic) {

        List<String> data = readS3Event(record.getS3().getBucket().getName(), record.getS3().getObject().getKey(), context);
        data.stream().forEach( item -> {
            context.getLogger().log("Publishing message : " + item);
            PublishRequest request = PublishRequest.builder()
                    .message(item)
                    .topicArn(snsTopic)
                    .build();
            snsClient.publish(request);
        });

    }

}
