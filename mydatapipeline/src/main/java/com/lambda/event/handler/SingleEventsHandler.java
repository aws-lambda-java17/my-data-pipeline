package com.lambda.event.handler;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.events.SNSEvent;

import java.io.PrintWriter;
import java.io.StringWriter;

public class SingleEventsHandler {

    public void handleEvents(SNSEvent event, Context context) {
        event.getRecords().forEach(rec -> {
            this.processSNSRecord(rec, context);
        });
    }

    private void processSNSRecord(SNSEvent.SNSRecord snsRecord, Context context) {

        try {
            String message = snsRecord.getSNS().getMessage();
            context.getLogger().log("Received SNS Message : \n" + message);

        } catch (Exception e) {
            StringWriter sw = new StringWriter();
            e.printStackTrace(new PrintWriter(sw));
            context.getLogger().log(sw.toString());

            throw new RuntimeException(e);
        }

    }

}
